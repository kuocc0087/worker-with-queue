﻿using System;
using System.Threading.Tasks;

namespace WorkerWithQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            var requestCount = 1;
            var isQuit = false;
            var worker = new Worker();
            var workerTask = worker.RunAsync();
            
            while (!isQuit)
            {
                Console.WriteLine("Enter how many seconds a new request should take to run or \"q\" to quit:");
                var command = Console.ReadLine().Trim().ToLower();
                isQuit = command.Equals("q");
                int duration = 0;

                if (isQuit)
                {
                    worker.Stop();
                    break;
                }

                if (int.TryParse(command, out duration))
                {
                    var request = new Request
                    {
                        Name = "Request " + requestCount,
                        Runtime = duration,
                    };
                    worker.AddToQueue(request);
                }

                requestCount++;
            }

            Console.WriteLine("Stopping...");
            while (!workerTask.IsCompleted) { }
            Console.WriteLine("Stopped");

            Task.Delay(2000).Wait();
        }
    }
}

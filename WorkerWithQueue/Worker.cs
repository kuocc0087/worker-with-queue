﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace WorkerWithQueue
{
    public class Worker : IDisposable
    {
        private BlockingCollection<Request> _queue = new BlockingCollection<Request>();

        public void AddToQueue(Request request)
        {
            _queue.Add(request);
            Console.WriteLine($"Added {request.Name}");
        }

        public void Dispose()
        {
            _queue.Dispose();
        }

        public async Task RunAsync()
        {
            Console.WriteLine("[x] Started Worker");

            var queueConsumerTask = Task.Run(async () =>
            {
                foreach (var request in _queue.GetConsumingEnumerable())
                {
                    Console.WriteLine($"Running {request.Name}");
                    await Task.Delay(request.Runtime * 1000);
                    Console.WriteLine($"Finished {request.Name}");
                }
            });
            await queueConsumerTask;

            Console.WriteLine("[x] Stopped Worker");
        }

        public void Stop()
        {
            _queue.CompleteAdding();
        }
    }
}
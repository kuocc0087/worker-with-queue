﻿namespace WorkerWithQueue
{
    public class Request
    {
        /// <summary>
        /// Name of the request
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Time the requests takes in seconds.
        /// </summary>
        public int Runtime { get; set; }
    }
}
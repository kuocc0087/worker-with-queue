﻿# Worker with a Queue using a `BlockingCollection`

A Windows Service application I had to build had several complex problems to solve, but namely related to how data was being mutated in the data layer. Basically, what the application had to do was accept requests to run complex stored procedures on the database. My option to drastically change the stored procedure to allow for concurrent processing and data creation was very limited as I did not own the SQL and time was limited. The following, along with the destructive data operations, presented me with a concurrency issue:

- Simultaneous requests
- Varying requests types (create and delete)
- Long running processes (one or multiple stored procedures)
- Asynchronous processes
- Unscheduled receipt of requests

To solve the long running processes problem, I decided to implement a worker thread with a queue of some sort. The worker thread would process the requests one at a time and pull them from the queue (FIFO). Being asynchronous, the worker thread could run as long as it needed while another thread or multiple threads could add more requests to the queue. This meant that the main thread would only create one instance of the worker thread and start it immediately and stop it only when the application stopped.

For the queue, I needed to obviously create a thread safe way of reading, removing, and adding requests. One typical way would be to create a lock when interacting with the queue, but I decided to use [`BlockingCollection`](https://docs.microsoft.com/en-us/dotnet/standard/collections/thread-safe/blockingcollection-overview) as it also handled one of the other minor problems I had besides thread safety. Namely, `BlockingCollection` implemented the Producer-Consumer pattern which seem to fit the problem of having inconsistent periods of time when requests may or may not be added to the queue.

Although I mentioned Windows Service above, this example will be a plain console app. I used .NET Core 2.2, but I believe that older versions of .NET Core will work and versions of .NET Framework 4.0 and greater as they should have the `BlockingCollection` class.

## Prerequisites

- .NET Core 2.2

## Creating a worker

First things first, create classes `Worker` and `Request` in your project.

```csharp

public class Worker {}

```

```csharp

public class Request
{
    /// <summary>
    /// Name of the request
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Time the requests takes in seconds.
    /// </summary>
    public int Runtime { get; set; }
}

```

In our `Request` class, we have name and runtime properties. We will use the runtime property in our worker to control how long a request is going to take to process to mock a long running operation.

The `Worker` class will need to add the following `using` to use the `BlockingCollection` class:

```csharp

using System.Collections.Concurrent;

```

Inside the `Worker`, we will create a field `_queue` and define it as a `BlockingCollection` that contains `Request` objects.

```csharp

public class Worker
{
    private BlockingCollection<Request> _queue = new BlockingCollection<Request>();

    public void AddToQueue(Request request)
    {
        _queue.Add(request);
        Console.WriteLine($"Added {request.Name}");
    }
}

```

We also are providing a method for adding `Request` objects to the `Worker._queue`.

That's good and all, but our `Worker` needs to do some work.

```csharp

// ...
using System.Threading.Tasks;

public class Worker
{
    // ...

    public async Task RunAsync()
    {
        Console.WriteLine("[x] Started Worker");

        var queueConsumerTask = Task.Run(async () =>
        {
            foreach (var request in _queue.GetConsumingEnumerable())
            {
                Console.WriteLine($"Running {request.Name}");
                await Task.Delay(request.Runtime * 1000);
                Console.WriteLine($"Finished {request.Name}");
            }
        });
        await queueConsumerTask;

        Console.WriteLine("[x] Stopped Worker");
    }
}

```

`BlockingCollection` contains a method called `GetConsumingEnumerable` which we can use `foreach` with to iterate the items added to the collection. For more information, you can find it [here](https://docs.microsoft.com/en-us/dotnet/standard/collections/thread-safe/how-to-use-foreach-to-remove). But basically it allows the caller of `GetConsumingEnumerable` to iterate, but will continue to loop for you until `BlockingCollection.CompleteAdding` is called. Each iteration will also be removed from the collection.

In the newly added `RunAsync` method added to the `Worker` class, what we are doing is creating a `Task` that treats the iteration of the `BlockingCollection` as a long running process that we can `await` on. With each iteration, we get a `Request` object that we can process. In this case, we take the object and delay/sleep for the amount of time provided in the `Request.Runtime` property we created earlier.

```csharp

public class Worker
{
    // ...

    public void Stop()
    {
        _queue.CompleteAdding();
    }
}

```

Finally, we will provide a way to indicate to the Worker that it should finish up the queue and stop. Before we completely finish though, we have to do some cleanup. We'll extend `IDisposable` and implement the `Dispose` method.

```csharp

public class Worker : IDisposable
{
    // ...

    public void Dispose()
    {
        _queue.Dispose();
    }
}

```

## Putting it to use

Now we have pieces of the puzzle that we need to put together and actually use. In our `Program` class that should have been generated when we created the project, we will be using our `Worker` and creating `Requests` and adding them dynamically.

```csharp

class Program
{
    static void Main(string[] args)
    {
        var requestCount = 1;
        var isQuit = false;
        var worker = new Worker();
        var workerTask = worker.RunAsync();
        
        while (!isQuit)
        {
            Console.WriteLine("Enter how many seconds a new request should take to run or \"q\" to quit:");
            var command = Console.ReadLine().Trim().ToLower();
            isQuit = command.Equals("q");
            int duration = 0;

            if (isQuit)
            {
                worker.Stop();
                break;
            }

            if (int.TryParse(command, out duration))
            {
                var request = new Request
                {
                    Name = "Request " + requestCount,
                    Runtime = duration,
                };
                worker.AddToQueue(request);
            }

            requestCount++;
        }

        Console.WriteLine("Stopping...");
        while (!workerTask.IsCompleted) { }
        Console.WriteLine("Stopped");

        Task.Delay(2000).Wait();
    }
}

```

Here, we are looping and prompting the user to enter how many seconds they want the request to take to run. The program will stop accepting request when the user enters "q". Finally, the main method will wait for the worker to complete processing the queue before it exits.

The following is the console input and output from an execution.

```sh

[x] Started Worker
Enter how many seconds a new request should take to run or "q" to quit:
5
Added Request 1
Enter how many seconds a new request should take to run or "q" to quit:
Running Request 1
4
Added Request 2
Enter how many seconds a new request should take to run or "q" to quit:
2
Added Request 3
Enter how many seconds a new request should take to run or "q" to quit:
3
Added Request 4
Enter how many seconds a new request should take to run or "q" to quit:
Finished Request 1
Running Request 2
Finished Request 2
Running Request 3
Finished Request 3
Running Request 4
Finished Request 4

```

You can see we are allowed to add as many request while a the first request is being processed. Then the subsequent requests will be processed in the order they were added.